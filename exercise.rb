class Exercise

  def self.marklar(str)
      arr = str.split(" ")

      arr.each do |w|
          w.sub!(/^[A-Z].{4}([^[:punct:]])*/,"Marklar") || w.sub!(/^[a-z].{4}([^[:punct:]])*/,"marklar")    
      end

      return arr.join(" ")
  end

  def self.even_fibonacci(nth)
      sum = 0
      fibs = [1,1]

      # only keeps necessary values so that memory usage is bound to O(1)
      while nth > 2 do
          current_fib = fibs[0] + fibs[1]
          current_fib.even? && sum += current_fib
          fibs[nth%2] = current_fib
          nth -= 1
      end

      return sum
  end

end
